class Esbstatus

	def initialize
		begin
			@@config = readEsbFile
		rescue
			Rails.logger.info 'no configuration file'
			@@config = nil
		end
	end

	def self.getESBStatus(orderid, creationdate)

		begin
		num = orderid.to_i
		if num <= 0
			return "error4"
		end

		unless creationdate.at(4) == "-" \
			and creationdate.at(7) == "-" \
			and creationdate.at(0..3).to_i > 2010 \
			and creationdate.at(5..6).to_i <= 12 \
			and creationdate.at(5..6).to_i > 0 \
			and creationdate.at(8..9).to_i <= 31 \
			and creationdate.at(8..9).to_i > 0 \
			and creationdate.length == 10
			return "error5"
		end

		Rails.logger.info 'getESBStatus: '
		if @@config.nil? ||@@config['endpoint'].nil? ||  @@config['username'].nil? || @@config['password'].nil? || @@config['wsdl'].nil?
			return "error8"
		end
		#config = readEsbFile
		req_url = @@config['endpoint']
		username = @@config['username']
		password = @@config['password']
		wsdl_url = @@config['wsdl']
		Rails.logger.info 'endpoint: ' + req_url
		Rails.logger.info 'username: ' + username
		Rails.logger.info 'password: ' + password
		Rails.logger.info 'wsdl: ' + wsdl_url

req = requestConfigurator(orderid, creationdate)
		client = Savon.client(wsdl: wsdl_url,
			open_timeout: 120,
			read_timeout: 120,
			headers: { "ESB-Username" => username, "ESB-Password" => password},
			logger: Rails.logger,
			log_level: :debug,
			log: true,
			pretty_print_xml: true,
			ssl_verify_mode: :none,
			endpoint: req_url)

			#response = client.call(:get_sap_delivery_status, soap_action: "/Services/CRMWebServicesSAPSO.serviceagent/PortTypeEndpoint1/GetSAPDeliveryStatus", xml: req)
			current_date = Time.new
			cdf = current_date.strftime("%Y-%m-%d")
			response = client.call(:get_sap_delivery_status, soap_action: "/Services/CRMWebServicesSAPSO.serviceagent/PortTypeEndpoint1/GetSAPDeliveryStatus", \
				:attributes => { "clientID" => "webshop" }, \
				:message => { \
					:timeStart => "00:00:00", \
					:timeEnd => "23:59:00", \
					:deliveryNumberSAP => orderid, \
					:dateStart => creationdate, \
			 		:dateEnd => cdf })
			res_hash = response.hash
			Rails.logger.info res_hash
			resp = res_hash[:envelope][:body][:get_sap_delivery_status_response][:response_body][:item]
			puts resp.length
			puts resp.class
			response_array = getShipment(resp)
			response_array.sort_by{ |obj| obj.datesorter }.reverse
		rescue
			return "error7"
	end
	end

	private
	def readEsbFile
		Rails.logger.info 'readEsbFile: '
		file = File.open(File.join(File.dirname(__FILE__), "../assets/status_configuration.txt")).read
		config = Hash.new
		file.each_line do |line|
			line = line.strip
			Rails.logger.info 'line: ' + line
			key, value = line.split '=', 2
			if key.include? "ESBwsdl" or "ESBendpoint" or key.include? "ESBpassword" or key.include? "ESBusername"
				keyvalue = key[3..-1]
				config[keyvalue] = value
				Rails.logger.info 'key: ' + keyvalue + ' ,value: ' + value
			end
		end
		return config
	end

	def self.requestConfigurator(orderid, creationdate)
		current_date = Time.new
		cdf = current_date.strftime("%Y-%m-%d")
		req = '<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:crm="http://www.vipnet.hr/xmlns/TIBCO/ESB/CRMServices">
					 <soap:Header/>
					 <soap:Body>
						<crm:GetSAPDeliveryStatusRequest clientID="?" externalTransactionID="?">
						 <timeStart>00:00:00</timeStart>
						 <timeEnd>23:59:59</timeEnd>
						 <deliveryNumberSAP>' + orderid + '</deliveryNumberSAP>
						 <dateStart>' + creationdate + '</dateStart>
						 <dateEnd>' + cdf + '</dateEnd>
						</crm:GetSAPDeliveryStatusRequest>
					 </soap:Body>
				</soap:Envelope>'
	end

	def self.getShipment(response)
		Rails.logger.info 'getShipment function'
		shipments = Array.new
		response.each do |item|
			Rails.logger.info 'getShipment item'
			shipment = Shipment.new
				Rails.logger.info 'getShipment new Shippment'
			shipment.SAPorderNumber = item[:sa_porder_number]
			shipment.externalStatus = item[:external_status]
			shipment.deliveryStatus = item[:delivery_status]
			shipment.expressDelCom = item[:express_del_com]
			shipment.datetime = dateformatter(item[:date],item[:time])
			shipment.datesorter = DateTime.strptime(shipment.datetime, "%d.%m.%Y. %H:%M")
			shipment.HUnumberExt = item[:h_unumber_ext]
			ad = item[:address][:item]
			address = Address.new
			address.name1 = ad[:name1]
			address.telNumber = ad[:tel_number]
			address.street = ad[:street]
			address.city = ad[:city]
			address.postalCode = ad[:postal_code]
			shipment.address = address
			shipments.push(shipment)
		end
		return shipments
	end

	def self.dateformatter(responsedate, responsetime)
		Rails.logger.info 'dateFormatter'
		deliverydate = responsedate[6..7] + '.' + responsedate[4..5] + '.' + responsedate[0..3] + '. ' + responsetime[0..1] + ':' + responsetime[2..3]
	end
end

status = Esbstatus.new
