class Status
	def getStatus(orderid)
		config = readFile
		req_url = config['endpoint']
		username = config['username']
		password = config['password']
		Rails.logger.info 'endpoint: ' + req_url
		Rails.logger.info 'username: ' + username
		Rails.logger.info 'password: ' + password
		
		req = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
			<soapenv:Header />
				<soapenv:Body>
					<Z_OSPVIP_SDB2C_ORDER_FLOW xmlns="urn:sap-com:document:sap:rfc:functions">
						<IMPORT xmlns="">2320034513</IMPORT>
						<I_ZZTRENN xmlns="">|</I_ZZTRENN>
						<RFC_CLIENT xmlns="">020</RFC_CLIENT>
					</Z_OSPVIP_SDB2C_ORDER_FLOW>
				</soapenv:Body>
			</soapenv:Envelope>'
		client = Savon.client(wsdl: 'C:\a1_tomato\sap\wsdl\order_update.wsdl', 
			open_timeout: 120, 
			read_timeout: 120, 
			basic_auth: [username, password],
			logger: Rails.logger,
			log_level: :debug,
			log: true,
			pretty_print_xml: true,
			ssl_verify_mode: :none,
			endpoint: config["endpoint"],
			env_namespace: 'http://schemas.xmlsoap.org/soap/envelope/')
		response = client.call(:z_ospvip_sdb2_c_order_flow, soap_action: "http://sap.com/xi/WebService/soap1.1", xml: req)
		res_hash = response.hash
	end
	
	def readFile
		file = File.open(File.join(File.dirname(__FILE__), "../assets/status_configuration.txt")).read
		config = Hash.new
		file.each_line do |line|
			line = line.strip
			key, value = line.split '=', 2
			if key.include? "endpoint" or key.include? "password" or key.include? "username"
				config[key] = value
			end
		end
		return config
	end
end