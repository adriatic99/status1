class Shipment
  attr_accessor :SAPorderNumber, :externalStatus, :deliveryStatus, :expressDelCom, :datetime, :datesorter, :HUnumberExt, :address
end

class Address
  attr_accessor :name1, :telNumber, :street, :city, :postalCode
end
