require 'savon'
require 'date'

class ApplicationController < ActionController::Base

	@@receive = Hash.new

	def orderinfo
		@sometext = 'hello'
		requesttime = DateTime.now
		begin
		@orderid = params[:id]
		creationdate = params[:creationdate]
		isorderok = isOrderProcessing(@orderid, requesttime)
		if isorderok == false
			Rails.logger.info "klikanje prebrzo"
			render status: 200
		end
		Rails.logger.info 'parameters: orderid: ' + @orderid + ' creationddate: ' + creationdate
		@status = Esbstatus.getESBStatus(@orderid, creationdate)
		if @status.instance_of? String
			raise 'An error has occured'
		end
		@current_status = @status.first
		@status.drop(1)
		rescue => e
			@errormsg = e.message
			unless 	@errormsg.empty?
				@errormsg = "nešto nevalja s parametrima"
			end
			render "error"
		end
	end

	private
	def isOrderProcessing(orderid, time)
		puts "isOrderProcessing orderid:"
		if @@receive[orderid].present?
			puts "orderid founded"
			previous = @@receive[orderid]
			puts previous
			puts time
			puts "before"
			diff = ((time - previous) * 24 * 60 * 60).to_i
			puts diff
			puts "after"
			@@receive[orderid] = time
			if(diff < 3)
				return false
			end
		else
			puts "orderid not founded"
			@@receive[orderid] = time
			return true
		end
	end
end
