FROM ruby:2.6.6
RUN apt-get update -qq && apt-get install -y nodejs yarn && apt-get install dos2unix && apt-get -y install sqlite3 libsqlite3-dev
RUN apt-get update && apt-get install -y \
  curl \
  build-essential \
  libpq-dev &&\
  curl -sL https://deb.nodesource.com/setup_10.x | bash - && \
  curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && \
  echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list && \
  apt-get update && apt-get install -y nodejs yarn
RUN mkdir /status1
WORKDIR /status1
RUN gem install bundler -v 2.1.4
COPY Gemfile /status1/Gemfile
COPY Gemfile.lock /status1/Gemfile.lock
RUN gem install minitest -v 5.14.0
RUN bundle install
COPY . /status1
RUN chown -R www-data:www-data /status1/
RUN chmod -R 777 /status1/
RUN chmod g+w /status1/Gemfile.lock
COPY yarn.lock /status1/yarn.lock
RUN yarn --version
RUN uname -r
RUN yarn install
# RUN yarn add bootstrap@4.3.1 jquery popper.js
# Add a script to be executed every time the container starts.
COPY entrypoint.sh /usr/bin/
RUN dos2unix /usr/bin/entrypoint.sh
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]
EXPOSE 3000
# Start the main process.
CMD ["rails", "server", "-b", "0.0.0.0"]
